# Pull base image.
FROM openjdk:8-stretch

# Install base software packages
RUN apt-get update && \
  apt-get install software-properties-common \
  wget \
  curl \
  git \
  lftp \
  unzip -y && \
  apt-get clean


# ——————————————————————————————
# Installs i386 architecture required for running 32 bit Android tools
# ——————————————————————————————

RUN dpkg --add-architecture i386 && \
  apt-get update -y && \
  apt-get install -y libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 && \
  rm -rf /var/lib/apt/lists/* && \
  apt-get autoremove -y && \
  apt-get clean


# ——————————————————————————————
# Installs Android SDK
# ——————————————————————————————

# The latest version can be view at this section: https://developer.android.com/studio/index.html#command-tools
ENV ANDROID_SDK_VERSION 4333796
ENV ANDROID_SDK_FILENAME sdk-tools-linux-${ANDROID_SDK_VERSION}.zip
ENV ANDROID_SDK_URL https://dl.google.com/android/repository/${ANDROID_SDK_FILENAME}
ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/tools/bin

RUN mkdir -p /root/.android && \
  touch /root/.android/repositories.cfg && \
  cd /opt && \
  wget -q ${ANDROID_SDK_URL} && \
  unzip ${ANDROID_SDK_FILENAME} -d ./android-sdk-linux && \
  rm ${ANDROID_SDK_FILENAME} && \
  yes | sdkmanager --licenses && \
  sdkmanager "tools" "platform-tools"  && \
  sdkmanager "platforms;android-28" "platforms;android-27" "platforms;android-26" "platforms;android-25" "platforms;android-23" && \
  sdkmanager "build-tools;28.0.3" "build-tools;27.0.3" "build-tools;27.0.0" "build-tools;26.0.2" "build-tools;25.0.3" "build-tools;25.0.2" "build-tools;25.0.0" && \
  sdkmanager "build-tools;23.0.2" "build-tools;23.0.3" "build-tools;23.0.1" && \
  sdkmanager "extras;android;m2repository" "extras;google;m2repository"

# ——————————————————————————————
# Installs Gradle
# ——————————————————————————————

# Gradle
ENV GRADLE_VERSION 5.4.1

RUN cd /usr/lib \
  && curl -fl https://downloads.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip -o gradle-bin.zip \
  && unzip "gradle-bin.zip" \
  && ln -s "/usr/lib/gradle-${GRADLE_VERSION}/bin/gradle" /usr/bin/gradle \
  && rm "gradle-bin.zip"

# Set Appropriate Environmental Variables
ENV GRADLE_HOME /usr/lib/gradle
ENV PATH $PATH:$GRADLE_HOME/bin
